package com.bluexmicro.module_componment

import android.os.SystemClock
import android.view.View
import com.bluexmicro.module_componment.MultipleTapBean.freezeTimeMillis
import com.bluexmicro.module_componment.MultipleTapBean.lastTriggerTime
import com.bluexmicro.module_componment.MultipleTapBean.mHash
import com.bluexmicro.module_componment.MultipleTapBean.threshold
import com.bluexmicro.module_componment.MultipleTapBean.timeArray
import com.bluexmicro.module_componment.ViewClickDelay.SPACE_TIME
import com.bluexmicro.module_componment.ViewClickDelay.hash
import com.bluexmicro.module_componment.ViewClickDelay.lastClickTime


/**
 * @author cg-zhong 2020/10/26
 * 描述：
 */
object ViewClickDelay {
    var hash: Int = 0
    var lastClickTime: Long = 0
    var SPACE_TIME: Long = 1000
}
//inline 赋值代码块，减少创建函数对象
/**
 * 1.防抖
 * 2.一定时间内点击多次，触发事件（类似Android系统里面的打开开发这模式的应用场景、点击两次退出应用）
 * infix 在编译阶段就会把代码片段copy到代码中，如果没有infix，在编译阶段就会生成一个方法对象（创建对象相对耗资源）
 *
 * @param spacingTime 如果事件响应的比较慢，可以设置较长的时间间隔
 */
inline fun <T : View> T.singleTap(spacingTime: Long = 500, crossinline clickAction: (T) -> Unit) {
    SPACE_TIME = spacingTime
    this.setOnClickListener {
        if (this.hashCode() != hash) {
            hash = this.hashCode()
            lastClickTime = System.currentTimeMillis()
            clickAction(this)
        } else {
            val currentTime = System.currentTimeMillis()
            if (currentTime - lastClickTime > SPACE_TIME) {
                lastClickTime = System.currentTimeMillis()
                clickAction(this)
            }
        }
    }
}

object MultipleTapBean {
    var mHash = 0
    var timeArray = Array(5) { 0L }//5次
    var lastTriggerTime = 0L //上一次触发的事件
    var freezeTimeMillis = 1500 //触发后冻结一段时间，禁止点击
    val threshold: Long = 3000//3s

}

/**
 * 一定时间内点击指定次数生效，例如进入开发者模式。5秒内点击5次
 */
inline fun <T : View> T.multiTap(crossinline block: (T) -> Unit) {
    setOnClickListener {
        if (this.hashCode() != mHash) {
            mHash = this.hashCode()
            timeArray = Array(5) { 0L }//5次
            lastTriggerTime = 0L
        }
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastTriggerTime > freezeTimeMillis) {
            System.arraycopy(timeArray, 1, timeArray, 0, timeArray.size - 1)
            //实现左移，然后最后一个位置更新距离开机的时间，如果最后一个时间和最开始时间小于DURATION，即连续5次点击
            timeArray[timeArray.lastIndex] = SystemClock.uptimeMillis() //System.currentTimeMillis()
            if (timeArray[timeArray.lastIndex] - timeArray[0] <= threshold) {
                lastTriggerTime = System.currentTimeMillis()
                block.invoke(this)
            }
        }
    }
}