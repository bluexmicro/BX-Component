package com.bluexmicro.module_componment.file

import android.os.Environment
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bluexmicro.module_componment.databinding.ItemInnerFolderBinding
import java.io.File

typealias OnItemClickCallback = (index: Int) -> Unit

class DirAdapter(diff: FileDiff = FileDiff()) : ListAdapter<File, FolderViewHolder>(diff) {

    private var cb: OnItemClickCallback? = null
    fun setOnItemClick(callback: OnItemClickCallback) {
        cb = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FolderViewHolder {
        return ItemInnerFolderBinding.inflate(LayoutInflater.from(parent.context), parent, false).run {
            FolderViewHolder(this)
        }
    }


    override fun onBindViewHolder(holder: FolderViewHolder, position: Int) {

        holder.binding.apply {
            val file = currentList[position]
            val rootDir = Environment.getExternalStorageDirectory()
            folderBtn.text = if (file.path == rootDir.path) {
//                root.context.getString(R.string.txt_root_folder)
                "Internal Storage"
            } else {
                file.name
            }

            folderBtn.setOnClickListener {
                cb?.invoke(holder.layoutPosition)
            }
        }
    }
}

class FolderViewHolder(val binding: ItemInnerFolderBinding) : RecyclerView.ViewHolder(binding.root)