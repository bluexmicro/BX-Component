package com.bluexmicro.module_componment.file

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.bluexmicro.module_componment.databinding.ActivityInnerChooseFileBinding
import com.bluexmicro.module_componment.singleTap
import kotlinx.coroutines.launch

class ChooseFileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInnerChooseFileBinding
    private val viewModel by viewModels<FileViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInnerChooseFileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        actionBar?.hide()
        supportActionBar?.hide()

        binding.backBtn.singleTap {
            finish()
        }

        lifecycleScope.launch {
            viewModel.result.collect {
                val intent = Intent()
                intent.putExtra("FileResult", it)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

        lifecycleScope.launch {
            viewModel.backEvent.collect {
                setResult(-1)
                finish()
            }
        }
    }

    companion object {

        //只能在onCreate中初始化
        fun getActivityResultContract(): ChooseOtaFile {
            return ChooseOtaFile()
        }

    }

}

class ChooseOtaFile : ActivityResultContract<Int, FileResult?>() {

    override fun createIntent(context: Context, input: Int?): Intent {
        val intent = Intent(context, ChooseFileActivity::class.java)
        intent.putExtra("requestCode", input ?: 0)
        return intent
    }


    override fun parseResult(resultCode: Int, intent: Intent?): FileResult? {
        return if (resultCode == Activity.RESULT_OK) {
            intent?.getSerializableExtra("FileResult") as FileResult?
        } else {
            null
        }
    }


}


