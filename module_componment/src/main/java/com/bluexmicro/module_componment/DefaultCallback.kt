package com.bluexmicro.module_componment

typealias OnItemClickCallback = (index: Int) -> Unit

interface OnItemListener<T> {

    fun onItemClick(t: T, index: Int)

    fun onItemLongClick(t: T, index: Int)

}