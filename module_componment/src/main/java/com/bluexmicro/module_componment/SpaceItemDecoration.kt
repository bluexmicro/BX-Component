package com.bluexmicro.module_componment

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SpaceItemDecoration(
    private val topDp: Int = 0,
    private val rightDp: Int = 0,
    private val bottomDp: Int = 0,
    private val leftDp: Int = 0,
) : RecyclerView.ItemDecoration() {


    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.top = dp2px(view.context, topDp).toInt()
        outRect.right = dp2px(view.context, rightDp).toInt()
        outRect.bottom = dp2px(view.context, bottomDp).toInt()
        outRect.left = dp2px(view.context, leftDp).toInt()

    }

    private fun dp2px(context: Context, dp: Int): Float = dp * context.resources.displayMetrics.density
}