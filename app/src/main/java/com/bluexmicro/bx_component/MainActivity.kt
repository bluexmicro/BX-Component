package com.bluexmicro.bx_component

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.bluexmicro.module_componment.file.ChooseFileActivity
import com.bluexmicro.module_componment.file.FilesFragment
import com.bluexmicro.module_componment.scan.ChooseDeviceActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


//        val launcher = registerForActivityResult(ChooseFileActivity.getActivityResultContract()) {
//            Log.e("onCreate: ", "?? ${it?.file?.path ?: ' '}")
//        }
//        lifecycleScope.launch {
//            delay(1000)
//            launcher.launch(0)
//        }

        val launcher = registerForActivityResult(ChooseDeviceActivity.getActivityResultContract()) {
            Log.e("onCreate: ", "?? ${it?.device?.address ?: ' '}")
        }
        lifecycleScope.launch {
            delay(1000)
            launcher.launch(0)
        }

    }


}